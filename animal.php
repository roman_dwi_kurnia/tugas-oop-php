<?php 

class animal{
    public $name;
    public $legs = 2;
    public $cold_blooded = "False";

    public function __construct($name){
        $this->name = $name;
    }
}

$sheep = new Animal("shaun");

echo "Nama         : " . $sheep->name . "<br>"; // "shaun"
echo "Legs         : " . $sheep->legs . "<br>"; // 2
echo "Cold-Bloodes : " . $sheep->cold_blooded . "<br>" . "<br>";// false

?>