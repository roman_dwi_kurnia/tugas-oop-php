<?php 

require_once ("animal.php");

class Frog extends animal{
    public $legs = 4;
    public function jump(){
        echo "hop hop";
    }
}

// index.php
$kodok = new Frog("buduk");
echo $kodok->jump(); // "hop hop"
echo "<br>";
echo "Nama            : " . $kodok->name . "<br>"; // "shaun"
echo "Legs            : " . $kodok->legs . "<br>"; // 2
echo "Cold-Bloodes    : " . $kodok->cold_blooded// false
?>