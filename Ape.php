<?php 

require_once ("animal.php");

class Ape extends animal{
    public $legs = 2;
    public function yell(){
        echo "Auooo";
    }
}

// index.php
$sungokong = new Ape("kera sakti");
echo "<br>" . "<br>";
echo $sungokong->yell(); // "Auooo"
echo "<br>";
echo "Nama            : " . $sungokong->name . "<br>"; // "shaun"
echo "Legs            : " . $sungokong->legs . "<br>"; // 2
echo "Cold-Bloodes    : " . $sungokong->cold_blooded // false

?>